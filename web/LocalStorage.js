/*     Created on : 11/10/2019, 09:05:20    Author     : haguilerts*/
class LocalStorage {

/* 2° */static guardar() {
            console.log("--hola estas en el metodo guardar...");
            let persona = {
                nombre: "haguilerts",
                edad: 23,
                correo: "hagui@gmail.com",
                DNI: 123456789
            };
            let nombre = "juan";
            localStorage.setItem("nombre:", nombre); // paso el nombre a BBDD WEB.
            localStorage.setItem("persona1:", JSON.stringify(persona));// paso persona en formato JSON a BBDD WEB.
            
            LocalStorage.obtener();// va a consultar
        }

/* 3° */static obtener() {
            console.log("--hola estas en el metodo obtener...");
            let nombreRecibido = localStorage.getItem("nombre:");
            let personaRecibido = localStorage.getItem("persona1:"); // llega en Json
            let personaRecibido2 = JSON.parse(localStorage.getItem("persona1:")); // lo trasnformo de json a objeto de java


            console.log("nombre: " + nombreRecibido); // imprime a juan 
            console.log("persona1(JSON): " + personaRecibido);// imprime a persona en formato JSON.
            console.log(personaRecibido2);// imprime a persona en formato objeto.

        }
/* 1° */static mail() {
        console.log("--hola estas en mail localStorage");
        document.querySelector("#BTlogin1").setAttribute("onclick","LocalStorage.guardar();"); // con un solo clip inserto y consulto       
        
    }
}
LocalStorage.mail();

///////////////////////////////////////////////////////////////////////////////////////////////////////


